using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinController : MonoBehaviour
{
    public GameManager gameManager;
    // Start is called before the first frame update
    void Start()
    {
        GameObject objeto = GameObject.Find("GameManager");
        gameManager = objeto.GetComponent<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "player")
        {
            Destroy(this.gameObject);
            gameManager.Puntuacion(10);
        }
    }
}
