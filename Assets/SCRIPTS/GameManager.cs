using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public float intervalo;
    public float tiempo;
    public TextMeshProUGUI textoPuntos;
    public TextMeshProUGUI textoEnd;
    public Button boton_tryagain;
    public Button boton_inicio;
    public Button boton_salir;
    public int puntos;
    public jugador player;

    // Start is called before the first frame update
    void Start()
    {
        tiempo = Time.time;
        Time.timeScale = 1;
    }

    // Update is called once per frame
    void Update()
    {
        if (player.vidas == 0)
        {
            Time.timeScale = 0;
            textoEnd.text = "GAME OVER";
            finpantalla();
        }
    }
    public void Puntuacion(int pts)
    {
        puntos = puntos + pts;
        textoPuntos.text = "Puntos: " + puntos;
    }
    public void Fin()
    {
        Time.timeScale = 0;
        textoEnd.text = "HAS GANADO!";
        finpantalla();
    }

    public void finpantalla()
    {
        boton_salir.gameObject.SetActive(true);
        boton_inicio.gameObject.SetActive(true);
        boton_tryagain.gameObject.SetActive(true);
    }


}
