using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class jugador : MonoBehaviour
{
    public float vel = 5.0f;
    public float correrVel = 8.0f;
    public float fuerzaSalto = 600f;

    public int vidas = 3;
    public Text textoVidas;
    public GameManager gameManager;

    public LayerMask capaSuelo;
    public Transform checkSuelo;

    bool EnSuelo;
    bool correr;
    bool dobleSalto;

    Rigidbody2D rb;
    Animator anim;
    Vector3 escalaPric;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
       // gameManager = GetComponent<GameManager>();
        escalaPric = transform.localScale;
        textoVidas.text = "Vidas: " + vidas;
    }

    private void Update()
    {
        float h;
        if (Input.GetKey(KeyCode.LeftShift))
        {
            correr = true;
        }
        else
        {
            correr = false;
        }

        if (correr)
        {
            h = Input.GetAxis("Horizontal") * correrVel;
        }
        else
        {
            h = Input.GetAxis("Horizontal") * vel;
        }


        rb.velocity = new Vector2(h, rb.velocity.y);

        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (EnSuelo)
            {
                rb.AddForce(Vector2.up * fuerzaSalto);
                anim.SetTrigger("Saltar");
            }
            else if (dobleSalto)
            {
                rb.velocity = Vector2.zero;
                rb.AddForce(Vector2.up * fuerzaSalto);
                anim.SetTrigger("Saltar");
                dobleSalto = false;
            }
        }

        if (rb.velocity.x > 0)
        {
            transform.localScale = escalaPric;
        }
        else if (rb.velocity.x < 0)
        {
            transform.localScale = new Vector3(-escalaPric.x, escalaPric.y, escalaPric.z);
        }

        // ANIMACIONES

        if (h != 0)
        {
            anim.SetBool("Andar", true);
        }
        else
        {
            anim.SetBool("Andar", false);
        }
        anim.SetBool("Correr", correr);
        anim.SetBool("EnSuelo", EnSuelo);

        // DOBLE SALTO

        if (EnSuelo)
            dobleSalto = true;
    }

    private void FixedUpdate()
    {
        EnSuelo = Physics2D.OverlapCircle(checkSuelo.position, 0.1f, capaSuelo);
    }
    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "enemigo")
        {
            GameOver();

        }
        if (collision.gameObject.name == "cofre")
        {
            gameManager.Fin();
           
        }
    }
    

    public void GameOver()
    {
        vidas = vidas - 1;
        textoVidas.text = "Vidas: " + vidas;
        if (vidas == 0)
        {
            Destroy(this.gameObject);
            textoVidas.text = "Vidas: " + vidas;
        }
    }
}
